
# Auto Insurance Quote

## How create the parent project

Create a new folder for the project (with the name quoting)

Create pom.xml file, with the following content:
```
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.example</groupId>
    <artifactId>quoting</artifactId>
    <version>1.0-SNAPSHOT</version>
    <packaging>pom</packaging>

    <modules>
    </modules>
</project>
```

Generate a .gitignore file from [https://gitignore.io](https://gitignore.io) and add to root folder

Create git repository and commit:
```
cd <root project path> # Enter to root project
git init # Init a git repository
git add :/ # Add the new files to git stagging area
git commit # Create a commit 
```

Create a Bitbucket repository with the following data:  
Repository name: quoting  
Access Level: public  
¿Create a README?:  No  
Control Version System: Git  
Project management: Issues management  
Language: Groovy

Push local changes
```
git remote add origin git@bitbucket.org:juancarlosmolinaserrano/quoting.git
git push origin master
```

## How create a business module

Create Groovy Maven project from [https://start.spring.io/](https://start.spring.io/) without choose any dependencies

Generate a .gitignore file from [https://gitignore.io](https://gitignore.io) and add to module root

Move src/main/groovy/com/example/quoting/QuotingBusinessApplication.groovy
to src/test/groovy/com/example/quoting

Move src/main/resources/application.properties to src/test/resource, to use only for tests
and avoid conflicts with a spring boot application

Delete CotizadorBusinessApplicationTests.groovy class

In 'pom.xml' file, remove the spring-boot-maven-plugin. Repackage phase is not need

Add a '.gitkeep' empty file (to avoid that git ignore empty folders) in:
src/main/groovy/com/example/quoting
src/main/resources

## How create a web module

Create Groovy Maven project from [https://start.spring.io/](https://start.spring.io/)
and choose web and devtools dependencies

Generate a .gitignore file from [https://gitignore.io](https://gitignore.io) and add to module root

Delete auto-generated test src/test/groovy/com/example/quoting/QuotingWebApplicationTests.groovy

Add a '.gitkeep' empty file (to avoid that git ignore empty folders) in:
src/test/groovy/com/example/quoting
src/test/resources

## How create a api module

Create Groovy Maven project from [https://start.spring.io/](https://start.spring.io/)
and choose web and devtools dependencies

Generate a .gitignore file from [https://gitignore.io](https://gitignore.io) and add to module root

Delete auto-generated test src/test/groovy/com/example/quoting/QuotingApiApplicationTests.groovy

Add a '.gitkeep' empty file (to avoid that git ignore empty folders) in:
src/test/groovy/com/example/quoting
src/test/resources

## How create a web functional tests module

Create module with the next command:
```
mvn archetype:generate -DgroupId=com.example -DartifactId=quoting-web-functional-tests -DarchetypeArtifactId=maven-archetype-quickstart -DarchetypeVersion=1.0 -DinteractiveMode=false
```

Generate a .gitignore file from [https://gitignore.io](https://gitignore.io) and add to module root

Create 'groovy/com/example/quoting' folders in src/main and src/test

Create 'resource' folders in src/main and src/test

Delete 'java' folders in src/main and src/test

In 'pom.xml' file add:
```
<dependency>
    <groupId>org.codehaus.groovy</groupId>
    <artifactId>groovy</artifactId>
</dependency>
```

and

```
<plugins>
    <plugin>
        <groupId>org.codehaus.gmavenplus</groupId>
        <artifactId>gmavenplus-plugin</artifactId>
        <version>1.5</version>
        <executions>
            <execution>
                <goals>
                    <goal>addSources</goal>
                    <goal>addTestSources</goal>
                    <goal>generateStubs</goal>
                    <goal>compile</goal>
                    <goal>testGenerateStubs</goal>
                    <goal>testCompile</goal>
                    <goal>removeStubs</goal>
                    <goal>removeTestStubs</goal>
                </goals>
            </execution>
        </executions>
    </plugin>
</plugins>
```

In 'pom.xml' (from parent project) add the other modules
```
<modules>
    <module>quoting-business</module>
    <module>quoting-api</module>
    <module>quoting-web</module>
    <module>quoting-web-functional-tests</module>
</modules>
```

Add a '.gitkeep' empty file (to avoid that git ignore empty folders) in:
src/main/groovy/com/example/quoting
src/main/resources
src/test/groovy/com/example/quoting
src/test/resources
