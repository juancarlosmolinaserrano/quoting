package com.example.quoting

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class QuotingApiApplication {

	static void main(String[] args) {
		SpringApplication.run(QuotingApiApplication, args)
	}

}
