#!/bin/bash

cd quoting-business && mvn -B clean install && cd ../
cd quoting-api && mvn -B clean verify && cd ../
cd quoting-web && mvn -B clean verify && cd ../
